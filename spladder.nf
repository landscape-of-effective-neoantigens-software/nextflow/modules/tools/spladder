process spladder_build {

  tag "${dataset}/${pat_name}/${run}"
  label 'spladder_container'
  label 'spladder_build'
  publishDir "${params.samps_out_dir}/${dataset}/${pat_name}/${run}/spladder_build"
  cache 'lenient'


  input:
  tuple val(pat_name), val(run), val(dataset), path(bam), path(bai)
  path gtf
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path("*_graph.json"), optional:true, emit: graph_jsons

  script:
  """
  spladder build \
    -a ${gtf} \
    --bams ${bam} \
    --parallel ${task.cpus} \
    -o ${dataset}-${pat_name}-${run}.spladder_build \
    ${parstr}
  """
}

process spladder_build_test {

  tag "${dataset}/${pat_name}/${norm_run}_${tumor_run}"
  label 'spladder_container'
  label 'spladder_build'
  publishDir "${params.samps_out_dir}/${dataset}/${pat_name}/${norm_run}_${tumor_run}/spladder_test"
  cache 'lenient'


  input:
  tuple val(pat_name), val(dataset), val(norm_run), path(norm_bam), path(norm_bai), val(tumor_run), path(tumor_bam), path(tumor_bai)
  path gtf
  val build_parstr
  val test_parstr

  output:
  tuple val(pat_name), val(dataset), val(norm_run), val(tumor_run), path("*_graph.json"), optional:true, emit: graph_jsons

  script:
  """
  spladder build \
    -a ${gtf} \
    --bams ${norm_bam},${tumor_bam} \
    --parallel ${task.cpus} \
    -o ${dataset}-${pat_name}-${norm_run}_${tumor_run}.spladder \
    ${build_parstr}
  spladder test \
    --conditionA ${tumor_bam} \
    --conditionB ${norm_bam} \
    --parallel ${task.cpus} \
    -o ${dataset}-${pat_name}-${norm_run}_${tumor_run}.spladder \
    ${test_parstr}
  """
}
